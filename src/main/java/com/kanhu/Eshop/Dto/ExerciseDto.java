package com.kanhu.Eshop.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ExerciseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1883524230217569994L;
	private Long grantId;
	private Long empId;
	private Double option;

	public Long getGrantId() {
		return grantId;
	}

	public void setGrantId(Long grantId) {
		this.grantId = grantId;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public Double getOption() {
		return option;
	}

	public void setOption(Double option) {
		this.option = option;
	}

	@Override
	public String toString() {
		return "ExerciseDto [grantId=" + grantId + ", empId=" + empId + ", option=" + option + "]";
	}

}
