package com.kanhu.Eshop.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.kanhu.Eshop.Constant.EsopConstant;
import com.kanhu.Eshop.Dto.ExerciseDto;
import com.kanhu.Eshop.Service.ExerciseActivityService;

@RestController
@RequestMapping(value = EsopConstant.FORWORDSLASH)
public class ExerciseActivityController {
	@Autowired
	private ExerciseActivityService exerciseActivityService;

	@PostMapping(value = EsopConstant.SELL_OPTION)
	public void sellOption(@RequestBody ExerciseDto exerciseDto) {
		exerciseActivityService.selloption(exerciseDto);
		System.out.println(exerciseDto);
	}
}
