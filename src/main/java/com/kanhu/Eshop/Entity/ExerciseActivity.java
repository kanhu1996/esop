package com.kanhu.Eshop.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.kanhu.Eshop.Constant.EsopConstant;

import lombok.Data;

@Data
@Entity
@Table(name = EsopConstant.EXERCISE_ACTIVITY_TABLE)
public class ExerciseActivity {
	@Id
	@GenericGenerator(name = "auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name = "id")
	private Long id;
	@Column(name = "garnt_Id")
	private Long garntId;
	@Column(name = "emp_Id")
	private Long empId;
	@Column(name = "sold_Option")
	private Double soldOption;
	@Column(name = "status")
	private String status;
	@Column(name = "created_Date")
	private Date createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGarntId() {
		return garntId;
	}

	public void setGarntId(Long garntId) {
		this.garntId = garntId;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public Double getSoldOption() {
		return soldOption;
	}

	public void setSoldOption(Double soldOption) {
		this.soldOption = soldOption;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "ExerciseActivity [id=" + id + ", garntId=" + garntId + ", empId=" + empId + ", soldOption=" + soldOption
				+ ", status=" + status + ", createdDate=" + createdDate + "]";
	}

}
