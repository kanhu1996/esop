package com.kanhu.Eshop.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.kanhu.Eshop.Dto.ExerciseDto;
import com.kanhu.Eshop.Entity.Exercise;
import com.kanhu.Eshop.Entity.ExerciseActivity;
import com.kanhu.Eshop.Entity.Grant;
import com.kanhu.Eshop.Repository.ExerciseActivityRepository;
import com.kanhu.Eshop.Repository.ExerciseReository;

@Service
public class ExerciseActivityService {
	@Autowired
	private ExerciseActivityRepository exerciseActivityRepository;
	@Autowired
	private ExerciseService exerciseService;
	@Autowired
	private JdbcTemplate jdbcTemple;
	@Autowired
	private ExerciseReository exerciseReository;

	public void selloption(ExerciseDto dto) {
		ExerciseActivity exerciseActivity = new ExerciseActivity();
		exerciseActivity.setGarntId(dto.getGrantId());
		exerciseActivity.setEmpId(dto.getEmpId());
		exerciseActivity.setStatus("Approved");
		exerciseActivity.setCreatedDate(new Date());
		List<Grant> grant = exerciseService.getObjectOfGrant(dto.getGrantId());
		Grant grant2 = grant.stream().filter(e -> e.getId().equals(dto.getGrantId())).collect(Collectors.toList())
				.get(0);
//		System.out.println(grant2);
		if (grant2.getLockInStatus().equals("open")) {
			List<Exercise> exercise = exerciseService.getObjectExercise(dto.getGrantId());
			Exercise exercise2 = exercise.stream().filter(e -> e.getGrantId().equals(dto.getGrantId()))
					.collect(Collectors.toList()).get(0);
//			System.out.println(exercise2);
			Double amount = exercise2.getVestingOption() - dto.getOption();
//			System.out.println(amount);
			if (amount >= dto.getOption()) {
				exerciseActivity.setSoldOption(dto.getOption());
				exercise2.setSoldOption(dto.getOption());
				exerciseReository.save(exercise2);
				exerciseActivityRepository.save(exerciseActivity);
			} else {
				System.out.println("Amount not to be sell");
			}
		} else {
			System.out.println("Grant lockstatus is  lock");
		}

	}
//	public void updatestaus(Long id) {
//		String sql="update exercise_activity_info set status='Aprroved' where id=?";
//		jdbcTemple.update(sql,id);
//	}
}
