package com.kanhu.Eshop.Helper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.kanhu.Eshop.Entity.Grant;

public class Helper {

	// check the file is excel or not
	public static boolean checkExcelFormate(MultipartFile file) {
		String contentType = file.getContentType();
		if (contentType.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
			return true;
		} else {
			return false;
		}
	}
//convert execle to list of grant

	public static List<Grant> convertexecleTOListOfGrant(InputStream is) {
		List<Grant> list = new ArrayList<Grant>();
		try {

			XSSFWorkbook workbook = new XSSFWorkbook(is);

			XSSFSheet sheet = workbook.getSheet("data");

			int rownumber = 0;
			Iterator<Row> iterator = sheet.iterator();

			while (iterator.hasNext()) {
				Row row = iterator.next();
				if (rownumber == 0) {
					rownumber++;
					continue;
				}
				Iterator<Cell> cells = row.iterator();
				int cid = 0;
				Grant grant = new Grant();
				while (cells.hasNext()) {
					Cell cell = cells.next();
					switch (cid) {
					case 0:
						grant.setId((long) cell.getNumericCellValue());
						break;
					case 1:
						grant.setEmployeeNumber(cell.getStringCellValue());
						break;
					case 2:
						grant.setBand((long) cell.getNumericCellValue());
						break;
					case 3:
						grant.setNumberOfGrants((long) cell.getNumericCellValue());
						break;

					case 4:
						grant.setGrantPrice(cell.getNumericCellValue());
						break;
					case 5:
						grant.setGrantStatus(cell.getStringCellValue());
						break;
					case 6:
						grant.setAccepted(cell.getBooleanCellValue());
						break;
					case 7:
						grant.setAcceptedDate(cell.getDateCellValue());
						break;
					case 8:
						grant.setLockInStatus(cell.getStringCellValue());
						break;
					case 9:
						grant.setFrequency((long) cell.getNumericCellValue());
						break;
					case 10:
						grant.setVestingTenure((long) cell.getNumericCellValue());
						break;
					case 11:
						grant.setAllocationStatus(cell.getStringCellValue());
						break;
					case 12:
						grant.setPlanId((long) cell.getNumericCellValue());
						break;
					default:
						break;
					}
					cid++;

				}
				list.add(grant);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
