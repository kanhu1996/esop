//package com.kanhu.Eshop.service;
//
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.util.ReflectionTestUtils;
//
//import com.kanhu.Eshop.Dto.OptionDto;
//import com.kanhu.Eshop.Dto.VestingOptionDto;
//import com.kanhu.Eshop.Entity.Plan;
//import com.kanhu.Eshop.Repository.PlanRepository;
//import com.kanhu.Eshop.Service.VestingOptionService;
//
//@RunWith(PowerMockRunner.class)
//public class VesingOptionServiseTest {
//	@Mock
//	private PlanRepository planRepository;
//	@Mock
//	private JdbcTemplate jdbcTemplate;
//	@Mock
//	private VestingOptionDto vestingOptionDto;
//	@Mock
//	private VestingOptionService vesingOptionServise;
//	@Mock
//	Plan plan;
//
//	@BeforeEach
//	public void beforeAll() {
//		MockitoAnnotations.initMocks(this);
//	}
//
//	@Test
//	public void updatePlanByVestingDtoTest() throws Exception {
//		PowerMockito.when(planRepository, "save", vestingOptionDto).thenReturn(plan);
//		ReflectionTestUtils.invokeMethod(vesingOptionServise, "updatePlanByVestingDto", plan);
//		assertTrue(true);
//	}
//
//	@Test
//	public void getpalnTest() throws Exception {
//		Plan plan = PowerMockito.mock(Plan.class);
//		Plan plan1 = PowerMockito.mock(Plan.class);
//		List<Plan> list = new ArrayList<Plan>();
//		list.add(plan);
//		list.add(plan1);
//		PowerMockito.when(jdbcTemplate, "query", "query", new BeanPropertyRowMapper<>(Plan.class)).thenReturn(list);
//		ReflectionTestUtils.invokeMethod(vesingOptionServise, "getpaln", 1L);
//		assertTrue(true);
//	}
//
//	@Test
//	public void calculateSumOfAllocationTest() throws Exception {
//		OptionDto optionDto = PowerMockito.mock(OptionDto.class);
//		OptionDto optionDto1 = PowerMockito.mock(OptionDto.class);
//		List<OptionDto> list = new ArrayList<>();
//		list.add(optionDto);
//		list.add(optionDto1);
//		PowerMockito.when(jdbcTemplate, "query", "query", new BeanPropertyRowMapper<>(Plan.class)).thenReturn(list);
//		ReflectionTestUtils.invokeMethod(vesingOptionServise, "calculateSumOfAllocation");
//		assertTrue(true);
//	}
//}
