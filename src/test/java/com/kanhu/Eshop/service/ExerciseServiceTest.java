//package com.kanhu.Eshop.service;
//
//import static org.junit.Assert.assertTrue;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.api.mockito.verification.PrivateMethodVerification;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.util.ReflectionTestUtils;
//
//import com.kanhu.Eshop.Entity.Exercise;
//import com.kanhu.Eshop.Repository.ExerciseReository;
//import com.kanhu.Eshop.Service.ExerciseService;
//import com.kanhu.Eshop.Service.VestingOptionService;
//
//@RunWith(value = PowerMockRunner.class)
//public class ExerciseServiceTest {
//	@Mock
//	private JdbcTemplate jdbcTemplate;
//	@Mock
//	private ExerciseReository exerciseReository;
//	@Mock
//	private ExerciseService exerciseService;
//	@Mock
//	private VestingOptionService vestingOptionService;
//
//	@Mock
//	Exercise mockExercise;
//
//	@BeforeEach
//	public void BeforeCall() {
//		MockitoAnnotations.initMocks(this);
//		mockExercise = PowerMockito.mock(Exercise.class);
//
//	}
//
//	@Test
//	public void getObjectExerciseTest() throws Exception {
//		Exercise mock1 = PowerMockito.mock(Exercise.class);
//		Exercise mock2 = PowerMockito.mock(Exercise.class);
//		List<Exercise> list = new ArrayList<Exercise>();
//		list.add(mock1);
//		list.add(mock2);
//		String sql = "SELECT * FROM exercise_info;";
//		PowerMockito.when(jdbcTemplate, "query", sql, new BeanPropertyRowMapper<>(Exercise.class)).thenReturn(list);
//		ReflectionTestUtils.invokeMethod(exerciseService, "getObjectExercise", 1L);
//		assertTrue(true);
//	}
//
//	@Test
//	public void updateExerciseTest() throws Exception {
//		ExerciseService mockservice = PowerMockito.mock(ExerciseService.class);
//		ExerciseService spy = PowerMockito.spy(mockservice);
//		PowerMockito.when(exerciseReository, "save", mockExercise).thenReturn(spy);
//		ReflectionTestUtils.invokeMethod(exerciseService, "updateExercise", 1L, mockExercise);
//		assertTrue(true);
//	}
//
//	@Test
//	public void saveExerciseTest() throws Exception {
//		ExerciseService mockservice = PowerMockito.mock(ExerciseService.class);
//		PowerMockito.when(exerciseReository, "save",mockExercise).thenReturn(mockExercise);
//		mockservice.saveExercise(mockExercise);
//	}
//}
