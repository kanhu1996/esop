//package com.kanhu.Eshop.service;
//
//import static org.junit.Assert.assertTrue;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.modules.junit4.PowerMockRunner;
//
//import org.springframework.test.util.ReflectionTestUtils;
//
//import com.kanhu.Eshop.Dto.ExerciseDto;
//
//import com.kanhu.Eshop.Entity.Exercise;
//import com.kanhu.Eshop.Entity.ExerciseActivity;
//import com.kanhu.Eshop.Repository.ExerciseActivityRepository;
//import com.kanhu.Eshop.Repository.ExerciseReository;
//import com.kanhu.Eshop.Service.ExerciseActivityService;
//
//@RunWith(value = PowerMockRunner.class)
//public class ExerciseActivityServiseTest {
//
//	@Mock
//	private ExerciseActivityService exerciseActivityService;
//	@Mock
//	private ExerciseActivityRepository exerciseActivityRepository;
//	@Mock
//	private ExerciseReository exerciseRepository;
//	@Mock
//	ExerciseActivity mockExerciseActivity;
//	@Mock
//	Exercise mockExercise;
//	@Mock
//	ExerciseDto Exercise;
//
//	@BeforeEach
//	public void BeforeCall() {
//		MockitoAnnotations.initMocks(this);
//		mockExerciseActivity = PowerMockito.mock(ExerciseActivity.class);
//		mockExercise = PowerMockito.mock(Exercise.class);
//	}
//
//	@Test
//	public void selloptionTest() throws Exception {
//		PowerMockito.when(exerciseRepository, "save", mockExercise).thenReturn(mockExercise);
//		PowerMockito.when(exerciseActivityRepository, "save", mockExerciseActivity).thenReturn(mockExerciseActivity);
//		ReflectionTestUtils.invokeMethod(exerciseActivityService, "selloption", Exercise);
//		assertTrue(true);
//	}
//}
