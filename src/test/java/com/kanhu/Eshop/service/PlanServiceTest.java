//package com.kanhu.Eshop.service;
//
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.util.ReflectionTestUtils;
//
//import com.kanhu.Eshop.Entity.Plan;
//import com.kanhu.Eshop.Repository.PlanRepository;
//import com.kanhu.Eshop.Service.PlanService;
//
//@RunWith(PowerMockRunner.class)
//public class PlanServiceTest {
//	@Mock
//	private PlanRepository planRepository;
//	@Mock
//	private JdbcTemplate jdbcTemplate;
//	@Mock
//	private PlanService planService;
//	@Mock
//	Plan plan;
//
//	@BeforeEach
//	public void beforeAll() {
//		MockitoAnnotations.initMocks(this);
//	}
//
//	@Test
//	public void savePlanTest() throws Exception {
//
//		PowerMockito.when(planRepository, "save", plan).thenReturn(plan);
//		ReflectionTestUtils.invokeMethod(planService, "savePlan", plan);
//		assertTrue(true);
//
//	}
//
//	@Test
//	public void findActivePlandByPlanYearTest() throws Exception {
//
//		List<Plan> list = new ArrayList<Plan>();
//		Plan plan = PowerMockito.mock(Plan.class);
//		Plan plan2 = PowerMockito.mock(Plan.class);
//		list.add(plan);
//		list.add(plan2);
//		PowerMockito.when(planRepository, "getPlanByPlanYear", "planYear").thenReturn(list);
//		ReflectionTestUtils.invokeMethod(planService, "findActivePlandByPlanYear", "2022");
//		assertTrue(true);
//	}
//
//	@Test
//	public void findByPlanIdTest() throws Exception {
//		PowerMockito.when(planRepository, "getReferenceById", 1L).thenReturn(plan);
//		ReflectionTestUtils.invokeMethod(planService, "findByPlanId", 1L);
//		assertTrue(true);
//	}
//
//	@Test
//	public void getCurrentActivePlanTest() throws Exception {
//		List<Plan> list = new ArrayList<Plan>();
//		Plan plan = PowerMockito.mock(Plan.class);
//		Plan plan2 = PowerMockito.mock(Plan.class);
//		list.add(plan);
//		list.add(plan2);
//		PowerMockito.when(jdbcTemplate, "query", "query", new BeanPropertyRowMapper<>(Plan.class)).thenReturn(list);
//		ReflectionTestUtils.invokeMethod(planService, "getCurrentActivePlan");
//		assertTrue(true);
//	}
//}
