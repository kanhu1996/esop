//package com.kanhu.Eshop.service;
//
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.util.ReflectionTestUtils;
//
//import com.kanhu.Eshop.Entity.Allocation;
//import com.kanhu.Eshop.Repository.AllocationRepository;
//import com.kanhu.Eshop.Service.allocationService;
//
//@RunWith(value = PowerMockRunner.class)
//public class AllocationServiceTest {
//	@Mock
//	private allocationService allocationService;
//
//	@Mock
//	private JdbcTemplate jdbctemple;
//
//	@BeforeEach
//	public void BeforeCall() {
//		MockitoAnnotations.initMocks(this);
//		Allocation mockAllocation = PowerMockito.mock(Allocation.class);
//	}
//
//	@Test
//	public void getByIdTest() throws Exception {
//		PowerMockito.when(jdbctemple, "update", "query", "id").thenReturn(1);
//		ReflectionTestUtils.invokeMethod(allocationService, "getById", 1L);
//		assertTrue(true);
//	}
//
//	@Test
//	public void approveAllocationTest() throws Exception {
//		List<Long> list = new ArrayList<Long>();
//		list.add(1L);
//		list.add(2l);
//		PowerMockito.doNothing().when(allocationService, "getById", 1L);
//		ReflectionTestUtils.invokeMethod(allocationService, "approveAllocation", list);
//		assertTrue(true);
//	}
//}
